<?php get_header(); ?>
<?php
$rewrite_rule = maybe_unserialize(get_option("cptui_post_types"));
$instocksyncseprate = "0";
if (get_option("instocksyncseprate") == "1") {
    $instocksyncseprate = "1";
}
$categories = array(
    "carpet" => array(
        "instock" => "instock_carpet",
        "normal" => "carpeting"
    ),
    "hardwood" => array(
        "instock" => "instock_hardwood",
        "normal" => "hardwood_catalog"
    ),
    "laminate" => array(
        "instock" => "instock_laminate",
        "normal" => "laminate_catalog"
    ),
    "tile" => array(
        "instock" => "instock_tile",
        "normal" => "tile_catalog"
    ),
    "lvt" => array(
        "instock" => "instock_lvt",
        "normal" => "luxury_vinyl_tile"
    ),
    "rugs" => array(
        "instock" => "instock_area_rugs",
        "normal" => "area_rugs"
    ),
    "paint" => array(
        "instock" => "paint_catalog",
        "normal" =>  "paint_catalog"
    )
);

$arr = array(
    'categories' => $categories,
    'rewrite_rules' => $rewrite_rule,
    'instocksyncseprate' => $instocksyncseprate
);
wp_localize_script('wp-product-filter-react', 'wpSearchProducts', $arr);
?>
<div class="fl-archive container ">
    <div class="row search-row">
        <h1 class="search-title">Search results for: <?php echo $_GET['s']; ?></h1>
        <div class="fl-content fl-builder-content" itemscope="itemscope" itemtype="http://schema.org/SearchResultsPage">

            <?php
            $postsgloblesearch = get_option("postsgloblesearch");
            if ($postsgloblesearch) {
                $args = array('post_type' => array('post', 'page'), 's' => $_GET['s'], 'post_per_page' => -1);
                $posts = new WP_Query($args);
                if ($posts->have_posts()) :
            ?>
                    <h2 class="search_category_title">Posts/Pages</h2>
                    <div class="posts_search_result post_list product-grid swatch row">
                        <?php
                        while ($posts->have_posts()) :
                            $posts->the_post();
                        ?>
                            <div class="product-item col-lg-4 col-md-4 col-sm-6 ">
                                <div class="fl-post-grid-post">
                                    <div class="fl-post-grid-image">
                                        <?php $url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'thumbnail') ? wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'thumbnail') : "https://placehold.co/300x300?text=No+Image"; ?>
                                        <a
                                            href="<?php the_permalink(); ?>"
                                            class=""><img src="<?php echo $url ?>" class="search_post_thumb" /></a>
                                    </div>
                                    <div class="fl-post-grid-text product-grid btn-grey">
                                        <h2><?php the_title(); ?></h2>
                                        <a
                                            href="<?php the_permalink(); ?>"
                                            class="fl-button plp_box_btn">
                                            View Post
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endwhile;
                        ?>
                    </div>
            <?php
                endif;
            }
            ?>

            <div id="search-result-page"></div>
        </div>
    </div>
</div>

<?php get_footer(); ?>